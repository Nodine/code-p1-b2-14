@echo off

if not exist "bin" mkdir "bin"
if not exist "out" mkdir "out"
if exist "out\tests.exe" del /F "out\tests.exe"
if exist "out\tests.exe" (
    @echo "Could not delete tests.exe"
    pause
    exit 1;
)

@echo Compiling ...
cd "src"
gcc -c linmath.c -o "..\bin\linmath.o"
gcc -c data.c -o "..\bin\data.o"
gcc -c simplex.c -o "..\bin\simplex.o"
cd "..\test"
gcc -c AllTests.c -o "..\bin\AllTests.o"
gcc -c CuTest.c -o "..\bin\CuTest.o"
gcc -c CuTestTest.c -o "..\bin\CuTestTest.o"
@echo Linking ...
cd "..\bin"
gcc linmath.o data.o simplex.o AllTests.o CuTest.o CuTestTest.o -o "..\out\tests.exe" 
@echo Done!

@echo Running tests ...
cd "..\out"
tests.exe
@pause
