@echo off

if not exist "bin" mkdir "bin"
if not exist "out" mkdir "out"
if exist "out\main.exe" del /F "out\main.exe"
if exist "out\main.exe" (
    @echo "Could not delete main.exe"
    pause
    exit 1;
)

@echo Compiling ...
cd "src"
gcc -g -c data.c -o "..\bin\data.o"
gcc -g -c linmath.c -o "..\bin\linmath.o"
gcc -g -c simplex.c -o "..\bin\simplex.o"
gcc -g -c main.c -o "..\bin\main.o"
@echo Linking ...
cd "..\bin"
gcc data.o linmath.o simplex.o main.o -o "..\out\main.exe" 
@echo Done!

@echo Cloning DB ...
cd ".."
xcopy /s /y "db" "out"
@echo Done!

cd "out"
if /I "%1"=="run" "main.exe"
if /I "%1"=="pause" pause
if /I "%2"=="pause" pause
