/**
 * @file
 * @brief Contains the Matrix structure and the functions to manipulate it.
 */

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
 
/* ==================== *
 *        MATRIX        *
 * ==================== */
/**
 * @author      Gruppe B2-14 <sw1b2-14@student.aau.dk>
 * @version     1.0
 */
typedef struct {
    /**
     * The number of columns in the matrix.
     */
    int columns;
    /**
     * The number of rows in the matrix.
     */
    int rows;
    /**
     * The zero-based data container.
     */
    double ** data;
} Matrix;

/**
 * Creates an mXn matrix where rows is m and n is columns.
 * @param rows The number of rows in the matrix.
 * @param columns The number of columns in the matrix.
 * @return A pointer to a Matrix.
 */
Matrix* createMatrix(int rows, int columns);

/**
 * Copies a matrix and all elements to a new matrix.
 * @param from The matrix to copy.
 * @return A pointer to a new Matrix.
 */
Matrix* copyMatrix(Matrix* from);

/**
 * Frees the memory used by the matrix.
 * @param matrix The matrix to be freed.
 */
void freeMatrix(Matrix* matrix);

/**
 * Copies a subset of the matrix.
 * @param matrix The Matrix to copy from.
 * @param row The row to start with.
 * @param column The column to start with.
 * @param rows The number of rows to copy.
 * @param columns The number of columns to copy.
 * @return A pointer to a new Matrix.
 */
Matrix* matrixCreateSubset(Matrix* matrix, int row, int column, int rows, int columns);

/**
 * Divides a row by a number.
 * @param matrix The matrix to perform the operation on.
 * @param row The row to perform the operation on.
 * @param value The number to divide by.
 */
void matrixRowDivide(Matrix* matrix, int row, double value);

/**
 * Subtracts a row from another row with a factor.
 * @param matrix The matrix to perform the operation on.
 * @param left The row that is subtracted from.
 * @param right The row that is subtracted by.
 * @param factor The number of times to subtract the row.
 */
void matrixRowSubtract(Matrix* matrix, int left, int right, double factor);

/**
 * Creates a pivot at the specified row and column.
 * @param row The row to create pivot.
 * @param column The column to create pivot.
 */
void matrixCreatePivot(Matrix* matrix, int row, int column);

/**
 * Finds the column with the smallest value in a row.
 * @param matrix The matrix to search.
 * @param row The row to search.
 * @return The column with the smallest value in the row.
 */
int matrixFindMinColumnInRow(Matrix* matrix, int row);

/**
 * Finds the row with the smallest value in a column.
 * @param matrix The matrix to search.
 * @param column The column to search.
 * @return The row with the smallest value in the column.
 */
int matrixFindMinRowInColumn(Matrix* matrix, int column);

void matrixDebugPrint(Matrix* matrix);