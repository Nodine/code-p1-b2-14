#include <stdio.h>

#include "data.h"
#include "linmath.h"
#include "string.h"

#define INGREDIENTS_PATH "ingredients.txt"
#define RECIPES_PATH "recipes.txt"

void printIngredients(Ingredient* ingredientRoot);
void printRecipes(Recipe* recipeRoot);
void createNewIngredient(Ingredient *root, int *numberOfIngredients);
void createNewRecipe(Recipe *root, Ingredient *ingredients, int *numberOfIngredients, int* numberOfRecipes);

int isQuitting = 0; /*Check whether*/
char input[80]; /*input buffer*/

/*Commands*/
char quitCommand[] = "quit"; 				/*Quit program*/
char madplanCommand[] = "madplan"; 			/*Generer madplan*/
char showCommand[] = "show"; 				/*Ask what to show*/
char removeCommand[] = "remove"; 			/*Ask what to remove*/
char createCommand[] = "create"; 			/*Ask what to create */
char ingredientsCommand[] = "ingredients"; 	/*Show, change or input new ingredients */
char recipesCommand[] = "recipes"; 			/*Show, change or input new Recipes */
char helpCommand[] = "help";				/*Shows the different commands and their purpose */

int main(void) {
    printf("Welcome to the mealplanner 9000\n");
	printf("Loading ingredients ... ");
	int numberOfIngredients;
	Ingredient *ingredients = readIngredients(INGREDIENTS_PATH, &numberOfIngredients);
	printf("Complete.\n");
    //printf("Ingredients before\n");
    //printIngredients(ingredients);
	printf("LoadingRecipes ... ");
	int numberOfRecipes;
	Recipe* recipes = readRecipes(RECIPES_PATH, ingredients, &numberOfIngredients, &numberOfRecipes);
    //printf("Ingredients after\n");
    //printIngredients(ingredients);
    //printRecipes(recipes, numberOfRecipes);
	printf("Complete.\n");
    
    // saving ingredients to update it with missing
    saveIngredients(INGREDIENTS_PATH, ingredients, numberOfIngredients);
    saveRecipes(RECIPES_PATH, recipes, numberOfRecipes);
    
    printf("Ingredients: %4d\n", numberOfIngredients);
    printf("Recipes:     %4d\n", numberOfRecipes);
	printf("Type 'help' for help\n");
    
    while(isQuitting == 0) {
		printf("Enter Command\n");
		scanf("%s", input);
		if(strcmp(input, quitCommand) == 0) {
			isQuitting = 1;
			printf("Exiting\n");
		} else if(strcmp(input, madplanCommand) == 0) {
            printf("Calculating foodplan ... ");
            Recipe** recipeArray;
            double* recipeMultiplier;
            int recipeCount;
            
            optimizeFoodplan(recipes->next, ingredients->next, &recipeArray, &recipeMultiplier, &recipeCount);
            printf("done!\n");
            printf(" === Plan === \n");
            printf("%8s %8s    %s\n", "Priority", "Number", "Recipe");
            int i, j;
            for(i = 0; i < recipeCount; i++) {
                double mult = recipeMultiplier[i];
                if(mult < DBL_EPSILON)
                    continue;
                printf("%8d %8.2lf    %s\n", recipePriority(recipeArray[i]), mult, recipeArray[i]->name);
            }
            printf(" === === \n");
            printf("Use plan?\n");
            printf("Y/N: ");
            scanf("%s", input);
            if(input[0] == 'y' || input[0] == 'Y') {
                for(i = 0; i < recipeCount; i++) {
                    double mult = recipeMultiplier[i];
                    if(mult < DBL_EPSILON)
                        continue;
                    for(j = 0; j < recipeArray[i]->ingredientCount; j++) {
                        recipeArray[i]->ingredientList[j]->amount -= mult * recipeArray[i]->ingredientAmount[j];
                        if(recipeArray[i]->ingredientList[j]->amount < DBL_EPSILON)
                            recipeArray[i]->ingredientList[j]->duration = 0.00;
                    }
                }
                saveIngredients(INGREDIENTS_PATH, ingredients, numberOfIngredients);
            }
		} else if(strcmp(input, showCommand) == 0) {
			printf("What would you like to show?\n");
			printf("You can show 'ingredients' or 'recipes'\n");
			scanf("%s", input);
			if(strcmp(input, ingredientsCommand) == 0) {
				printIngredients(ingredients);
			} else if (strcmp(input, recipesCommand) == 0) {
				printRecipes(recipes);
			}
		} else if(strcmp(input, removeCommand) == 0) {
			printf("What would you like to remove?\n");
			printf("You can remove 'ingredients' or 'recipes'\n");
			scanf("%s", input);
			if(strcmp(input, ingredientsCommand) == 0) {
				printf("Current ingredient list: \n");
				printIngredients(ingredients);
				printf("Which ingredient would you like to remove?\n");
				scanf(" %[^\n]", input);
                Ingredient* ingredient = findIngredientNodeByName(ingredients, input);
                if(ingredient != NULL) {
                    ingredient->amount = 0;
                    ingredient->duration = 0.00;
                    printf("Ingredient 'removed'!\n");
                    saveIngredients(INGREDIENTS_PATH, ingredients, numberOfIngredients);
                } else {
                    printf("Invalid ingredient!\n");
                }
			} else if (strcmp(input, recipesCommand) == 0) {
				printf("Current recipe list: \n");
				printRecipes(recipes);
				printf("Which recipe would you like to remove?\n");
				scanf(" %[^\n]", input);
                Recipe* recipe = findRecipeNodeByName(recipes, input);
                if(recipe != NULL) {
                    removeRecipeNode(recipes, recipe, &numberOfRecipes);
                    printf("Recipe removed!\n");
                    saveRecipes(RECIPES_PATH, recipes, numberOfRecipes);
                } else {
                    printf("Invalid recipe!\n");
                }
			}
		} else if(strcmp(input, createCommand) == 0) {
			printf("What would you like to create?\n");
			printf("You can create 'ingredients' or 'recipes'\n");
			scanf("%s", input);
			if(strcmp(input, ingredientsCommand) == 0) {
				createNewIngredient(ingredients, &numberOfIngredients);
                saveIngredients(INGREDIENTS_PATH, ingredients, numberOfIngredients);
			} else if (strcmp(input, recipesCommand) == 0) {
				createNewRecipe(recipes, ingredients, &numberOfIngredients, &numberOfRecipes);
                saveIngredients(INGREDIENTS_PATH, ingredients, numberOfIngredients);
                saveRecipes(RECIPES_PATH, recipes, numberOfRecipes);
			}
		} else if(strcmp(input, helpCommand) == 0) {
			printf("-----------------------\n");
			printf("Valid commands:\n");
			printf("quit\n");
			printf("madplan\n");
			printf("show\n");
			printf("remove\n");
			printf("create\n");
			printf("-----------------------\n");
		} else{
			printf("Not valid\n");
			printf("Try again\n");
		}
	}
    return 0;
}

void printIngredients(Ingredient* ingredientRoot) {
	int i = 0;
	Ingredient *ingredient = ingredientRoot;
	while(ingredient->next != NULL) {
		ingredient = ingredient->next;
		printf("%2d) %s, %d gram, %1.0f dages holdbarhed. \n", i + 1, ingredient->name, ingredient->amount, ingredient->duration);
		i++;
	}
}

void printRecipes(Recipe* recipeRoot) {
	int i = 0;
	Recipe *recipe = recipeRoot;
	while(recipe->next != NULL) {
		recipe = recipe->next;
		printf("%d) %s\n", i + 1, recipe->name);
		int j = 0;
		for (j = 0; j < recipe->ingredientCount; j++)
			printf("  %d) %s, %d gram \n", j + 1, recipe->ingredientList[j]->name, recipe->ingredientAmount[j]);
		i++;
	}
}

void createNewIngredient(Ingredient *root, int *numberOfIngredients) {
	char *newIngredientName = malloc(100);
	int newIngredientAmount;
	double newIngredientDuration;
    Ingredient* ingredient;
	
	int i = 0;
	printf("What is the name of the ingredient?\n");
	scanf(" %[^\n]", newIngredientName);
    ingredient = findIngredientNodeByName(root, newIngredientName);
    if(ingredient != NULL) {
        printf("Ingredients exists, the following input is added to:\n");
		printf("%s, %d gram, %1.0f days\n", ingredient->name, ingredient->amount, ingredient->duration);
    }
	printf("How much of the new ingredient is there (grams)?\n");
	scanf("%d", &newIngredientAmount);
	printf("How long can this ingredient last (days)?\n");
	scanf("%lf", &newIngredientDuration);
    
    if(ingredient == NULL) {
        ingredient = allocIngredient(newIngredientName, 0, 0.00);
        addIngredientNode(root, ingredient, numberOfIngredients);
    }
    ingredient->amount += newIngredientAmount;
    ingredient->duration += newIngredientDuration;
    printf("%s, %d gram, %1.0f days\n", ingredient->name, ingredient->amount, ingredient->duration);
}

void createNewRecipe(Recipe *root, Ingredient *ingredients, int *numberOfIngredients, int* numberOfRecipes) {
	int i;
    
    char recipeName[100];
	printf("What is the name of the recipe?\n");
	scanf(" %[^\n]", recipeName);
    
	int recipeIngredientCount = 0;
	printf("What is the number of ingredients?\n");
	scanf("%d", &recipeIngredientCount);
    
	int *recipeIngredientAmount = malloc(recipeIngredientCount * sizeof(int));
	Ingredient **recipeIngredientList = malloc(recipeIngredientCount * sizeof(Ingredient*));
    
	char ingredientName[100];
    int ingredientAmount;
	for(i = 0; i < recipeIngredientCount; i++) {
		printf("What is the name of one of the ingredients?\n");
		scanf(" %[^\n]", ingredientName);
		recipeIngredientList[i] = findIngredientNodeByName(ingredients, ingredientName);
		if(recipeIngredientList[i] == NULL)
            recipeIngredientList[i] = addIngredientNode(ingredients, allocIngredient(ingredientName, 0.00, 0.00), numberOfIngredients);
        
        printf("How much of this ingredient is needed for the dish?\n");
		scanf("%d", &ingredientAmount);
		recipeIngredientAmount[i] = ingredientAmount;
    }
    
    Recipe* newRecipe = allocRecipe(recipeName, recipeIngredientCount);
    
    free(newRecipe->ingredientList); // free preallocated space
	newRecipe->ingredientList = recipeIngredientList;
    free(newRecipe->ingredientAmount); // free preallocated space
	newRecipe->ingredientAmount = recipeIngredientAmount;
    
	addRecipeNode(root, newRecipe, numberOfRecipes); 
}