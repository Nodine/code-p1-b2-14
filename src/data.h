/**
 * @file
 * @brief Contains the Ingredient and Recipe structures and the functions to manipulate them.
 */

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define SEPERATOR_SIZE 1
 
/* ==================== *
 *      INGREDIENT      *
 * ==================== */
/**
 * @author      Gruppe B2-14 <sw1b2-14@student.aau.dk>
 * @version     1.0
 */
struct Ingredient {
    /**
     * The name.
     */
	char *name;
    /**
     * The amount in grams.
     */
	int amount;
    /**
     * The number of days until expiration.
     */
	double duration;
    
    /**
     * The next ingredient in the linked list or NULL.
     */
	struct Ingredient *next;
};
typedef struct Ingredient Ingredient;

/**
 * Allocated and initializes an ingredient.
 * @param name The name of the ingredient. NULL is an acceptable value.
 * @param amount The amount (grams) of the ingredient.
 * @param duration The number of days until the ingredient expires.
 * @return The pointer to the ingredient.
 */
Ingredient* allocIngredient(char* name, int amount, double duration);

/**
 * Loads a linked list of ingredients from a file.
 * @param filePath Path to a the file.
 * @param numberOfIngredients An output parameter containing the number of ingredients loaded.
 * @return The root of a linked list of Ingredient.
 */
Ingredient *readIngredients(const char* filePath, int *numberOfIngredients);

/**
 * Saves a linked list of ingredients into a file.
 * @param filePath Path to a the file.
 * @param ingredients The first ingredient in a linked list.
 * @param numberOfIngredients The number of ingredients to be saved.
 */
void saveIngredients(const char* filePath, Ingredient* ingredients, int numberOfIngredients);

/**
 * Finds an ingredient with the specified name.
 * @param root The root or first node of a linked list of Ingredient.
 * @param name The name to find.
 * @return The pointer to the ingredient.
 */
Ingredient *findIngredientNodeByName(Ingredient *root, char *name);

/**
 * Adds an Ingredient node to a linked list of Ingredient.
 * @param root A node from a linked list of Ingredient.
 * @param node The node to add.
 * @param numberOfIngredients The number of ingredients in the linked list. This is updated.
 * @return The pointer to the ingredient.
 */
Ingredient *addIngredientNode(Ingredient *root, Ingredient* node, int *numberOfIngredients);

/**
 * Removes an Ingredient node from a linked list of Ingredient.
 * @param root A node, before the node to be removed, from a linked list of Ingredient.
 * @param node The node to remove.
 * @param numberOfIngredients The number of ingredients in the linked list. This is updated.
 */
void removeIngredientNode(Ingredient *root, Ingredient* node, int *numberOfIngredients);
 
/* ==================== *
 *        RECIPE        *
 * ==================== */
/**
 * @author      Gruppe B2-14 <sw1b2-14@student.aau.dk>
 * @version     1.0
 */
struct Recipe {
    /**
     * The name.
     */
	char *name;
    /**
     * The number of ingredients.
     */
	int ingredientCount;
    /**
     * The list of ingredients.
     */
    Ingredient **ingredientList;
    /**
     * The amount of each ingredient in the list.
     */
	int *ingredientAmount;
    
    /**
     * The next recipe in the linked list or NULL.
     */
    struct Recipe *next;
};
typedef struct Recipe Recipe;

/**
 * Allocated and initializes an recipe.
 * @param name The name of the recipe. NULL is an acceptable value.
 * @param ingredientCount The number of ingredients. 0 is an acceptable value.
 * @return The pointer to the recipe.
 */
Recipe* allocRecipe(char* name, int ingredientCount);

/**
 * Loads a linked list of recipes from a file.
 * @param filePath Path to a the file.
 * @param ingredient A linked list of ingredients. Is updated if new ingredients are added.
 * @param numberOfIngredients The number of ingredients in the linked list. Is updated if new ingredients are added.
 * @param numberOfRecipes An output parameter containing the number of recipes loaded.
 * @return The root of a linked list of Recipe.
 */
Recipe *readRecipes(const char* filePath, Ingredient *ingredient, int *numberOfIngredients, int* numberOfRecipes);

/**
 * Saves a linked list of recipes into a file.
 * @param filePath Path to a the file.
 * @param recipes The first recipe in a linked list.
 * @param numberOfIngredients The number of recipes to be saved.
 */
void saveRecipes(const char* filePath, Recipe *recipes, int numberOfIngredients);

/**
 * Finds a recipe with the specified name.
 * @param root The root or first node of a linked list of Recipe.
 * @param name The name to find.
 * @return The pointer to the recipe.
 */
Recipe *findRecipeNodeByName(Recipe *root, char *name);

/**
 * Adds a Recipe node to a linked list of Recipe.
 * @param root A node from a linked list of Recipe.
 * @param node The node to add.
 * @param numberOfRecipes The number of recipes in the linked list. This is updated.
 * @return The pointer to the recipe.
 */
Recipe *addRecipeNode(Recipe *root, Recipe* node, int *numberOfRecipes);

/**
 * Removes a Recipe node from a linked list of Recipe.
 * @param root A node, before the node to be removed, from a linked list of Recipe.
 * @param node The node to remove.
 * @param numberOfRecipes The number of recipes in the linked list. This is updated.
 */
void removeRecipeNode(Recipe *root, Recipe* node, int *numberOfRecipes);
