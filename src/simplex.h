/**
 * @file
 * @brief Contains the tools to setup and solve a Simplex matrix.
 */

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <float.h>

#include "data.h"
#include "linmath.h"

/**
 * Creates and setups a Matrix based on the Recipe and Ingredient.
 * @param rootRecipe The first node of a linked list of Recipe.
 * @param rootIngredient The first node of a linked list of Ingredient.
 * @return A pointer to a Matrix.
 */
Matrix* createSimplexMatrix(Recipe* rootRecipe, Ingredient* rootIngredient);

/**
 * Solves a simplex Matrix.
 * @param matrix The matrix to solve.
 * @return The matrix that was passed.
 */
Matrix* solveSimplexMatrix(Matrix* matrix);

/**
 * Runs Recipes and Ingredients through the simplex approach to optimize it.
 * @param rootRecipe The first node of a linked list of Recipes.
 * @param rootIngredient The first node of a linked list of Ingredient.
 * @param recipes An output paramter holding an array of Recipe pointer.
 * @param recipeMultiplier An output paramter holding how much to make of each recipe.
 * @param recipeCount An output paramter holding the number of recipes in the optimization.
 */
void optimizeFoodplan(Recipe* rootRecipe, Ingredient* rootIngredient, Recipe*** recipes, double** recipeMultiplier, int* recipeCount);