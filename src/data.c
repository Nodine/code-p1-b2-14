#include "data.h"

int digits(int number);

/*Finder ingrediens listen og skaber en linked list ud af hele listen.
 * skal bruge en path: char *path = "ingredientList.txt"; */
Ingredient* readIngredients(const char* filePath, int* numberOfIngredients) { 
	/* Prepares file for reading */
	FILE *ingredientList = fopen(filePath, "r");
	/* Storage for the read characters */
	char ingredientString[256];
	/* Counting variables */
	int i = 0, j = 0;
	/* Allocates space for the root of the linked list */
	Ingredient *root = allocIngredient(NULL, 0.00, 0.00);
	/* Creates a conductor to move through the linked list */
	Ingredient *conductor = root;
	
	/*checking if the file is available to be read. */
	if(ingredientList == NULL) {
		printf("error opening file, quitting\n");
		exit(-1);
	}
	/* Checks the number on the first line of the ingredient list as it contains
	 * the current amount of ingredients in the list */
	*numberOfIngredients = atoi(fgets(ingredientString, 256, ingredientList));
    
	if (*numberOfIngredients == 0) {
		/* if the amount of ingredients is 0, end. */
		printf("No ingredients in file, Quitting\n");
		exit(-1);
	}
	/*Runs while the next line in the ingredientlist still exists and
	 * the list contained ingredients in the first place */
	while(fgets(ingredientString, 256, ingredientList) != NULL && i < *numberOfIngredients) {
		char ingredientName[100];
		int ingredientAmount;
        double ingredientDuration;
		/* Scans the string gotten via fgets in the beginning of the while loop,
		 * and saves the content to the specified parameters */
		sscanf(ingredientString, "%s %d %lf", ingredientName, &ingredientAmount, &ingredientDuration);
		/* Creates the next node of the linked list */
        conductor->next = allocIngredient(ingredientName, ingredientAmount, ingredientDuration);
		/* Moves the conductor to the next node to begin work */
		conductor = conductor->next;
		
		/* removes the non white space chars and replaces them with spaces, for read-ability */
        j = 0;
		while (conductor->name[j] != '\0') {
			if(conductor->name[j] == '-')
				conductor->name[j] = ' ';
			j++;
		}
		i++;
	}
	
	/* Closes the file again */
	fclose(ingredientList);
	/* This function only returns the head of the linked list as nothing more is needed */
	return root;
}

/* saves all the ingredients */
void saveIngredients(const char* filePath, Ingredient* rootIngredient, int numberOfIngredients) {
	/* opens the file to be read */
	FILE *ingredientList = fopen(filePath, "w");
	/* Counting variables */
	int i, j = 0;
	/* Pointer used to move through the linked list */
	Ingredient *conductor = rootIngredient;
	/* moves the conductor on from the root as the root does not contain useful data */
	conductor = conductor->next;
	
	/* prints the amount of ingredients in the linked list to the first line of the text file */
	fprintf(ingredientList, "%d\n", numberOfIngredients);
	/* Moves through the whole of the linked list */
	while(conductor != NULL) {
		char tempName[256];
        strcpy(tempName, conductor->name);
        /* save convention. */
		j = 0;
		/* Removes the spaces in the ingredient names and replaces them with dashes */
		while (tempName[j] != '\0') {
			if(tempName[j] == ' ')
				tempName[j] = '-';
			j++;
		}
		/* prints the name, amount and duration to the current line of the text file */
		fprintf(ingredientList, "%s %d %1.2lf\n", tempName, conductor->amount, conductor->duration);
		/* Moves the conductor further along the linked list */
		conductor = conductor->next;
	}
	printf("New ingredient list saved successfully!\n");
	/* Removes the file from memory */
	fclose(ingredientList);
}

/* this returns a linked list of available recipes. */
Recipe *readRecipes(const char* filePath, Ingredient* ingredients, int* numberOfIngredients, int* numberOfRecipes) {
	/* Prepares file for reading */
	FILE *recipeList = fopen("recipes.txt", "r");
	/* Storage for the read characters */
	char recipeString[256];
	/* Counting variables */
	int i, j;
	/* Allocates space for the root of the linked list */
	Recipe *root = allocRecipe(NULL, 0);
	/* Creates a conductor to move through the linked list */
	Recipe *conductor = root;
	
	/* Checking if the file is available to be read. */
	if(recipeList == NULL) {
		printf("File does not exist, Quitting\n");
		exit(-1);
	}
	
	/* Checks the number on the first line of the recipe list as it contains
	 * the current amount of recipes in the list */
	*numberOfRecipes = atoi(fgets(recipeString, 256, recipeList));
    
	if (*numberOfRecipes == 0) {
		/* if the first number is 0, there are no recipes */
		printf("No recipes in file, Quitting\n");
		exit(-1);
	}
	/* Runs while the next line in the recipeList still exists and
	 * the list contained recipes in the first place */
	while(fgets(recipeString, 256, recipeList) != NULL) {
		char recipeName[256];
		int ingredientCount;
		
		/* Gets the number of ingredients in the recipe and the name of it */
		sscanf(recipeString, "%d %s", &ingredientCount, recipeName);
		/* Allocates space for the next node in the list */
		conductor->next = allocRecipe(recipeName, ingredientCount);
		/* Moves the conductor to the next node */
		conductor = conductor->next;
		
		/* Removes some white space characters and replaces them with spaces */
		for(j = 0; j < strlen(conductor->name); j++) {
			if(conductor->name[j] == '-')
				conductor->name[j] = ' ';
		}
		/* strLength is used to make sscanf start further in the current line to first find the ingredient amount
		 * the name, and then all of the different ingredients and their amounts */
		/* digits converts an integer to the number of digits they take up eg.:
		 * 1 is 1, 10 is 2, 100 is 3 */
		int strLength = digits(ingredientCount) + SEPERATOR_SIZE + strlen(conductor->name) + SEPERATOR_SIZE;
		
		/* Goes through every ingredient a single recipe contains */
		for(i = 0; i < ingredientCount; i++){
			/* Allocates space for the name of the ingredient */
			char ingredientName[256];
			int ingredientAmount;
			/* Scans for and saves the name of the ingredient and its amount */
			sscanf(recipeString + strLength, "%s %d", ingredientName, &ingredientAmount);
            for(j = 0; j < strlen(ingredientName); j++) {
                if(ingredientName[j] == '-')
                    ingredientName[j] = ' ';
            }

			/* Finds the ingredient in the ingredient list and saves it to the ingredient list */
            conductor->ingredientList[i] = findIngredientNodeByName(ingredients, ingredientName);
            if(conductor->ingredientList[i] == NULL)
                conductor->ingredientList[i] = addIngredientNode(ingredients, allocIngredient(ingredientName, 0.00, 0.00), numberOfIngredients);

			/* Saves the amount of the current ingredient to the array of amounts */
			conductor->ingredientAmount[i] = ingredientAmount;
			/* Updates the strLength variable to increase the offset */
			strLength = strLength + digits(conductor->ingredientAmount[i]) + SEPERATOR_SIZE + strlen(ingredientName) + SEPERATOR_SIZE;
		}
	}
	fclose(recipeList);
	return root;
}

/* saves all the recipes */
void saveRecipes(const char* filePath, Recipe *recipes, int numberOfRecipes) {
	/* opens the file to be read */
	FILE *recipeFile = fopen(filePath, "w"); 
	/* Counting variables */
	int i, j = 0;
	/* Pointer used to move through the linked list */
	Recipe *conductor = recipes;
	
	/* prints the amount of ingredients in the linked list to the first line of the text file */
	fprintf(recipeFile, "%d\n", numberOfRecipes);
	/* Moves through the whole of the linked list */
	while(conductor->next != NULL){
        conductor = conductor->next;
        char tempName[256];
        strcpy(tempName, conductor->name);
        
		/* save convention. */
		j = 0;
		/* Removes the spaces in the ingredient names and replaces them with dashes */
		while (tempName[j] != '\0') {
			if(tempName[j] == ' ')
				tempName[j] = '-';
			j++;
		}
		/* prints the name, weight and duration to the current line of the text file */
		fprintf(recipeFile, "%d %s", conductor->ingredientCount, tempName);
        for(i = 0; i < conductor->ingredientCount; i++) {
            strcpy(tempName, conductor->ingredientList[i]->name);
            /* save convention. */
            j = 0;
            /* Removes the spaces in the ingredient names and replaces them with dashes */
            while (tempName[j] != '\0') {
                if(tempName[j] == ' ')
                    tempName[j] = '-';
                j++;
            }
            fprintf(recipeFile, " %s %d", tempName, conductor->ingredientAmount[i]);
        }
		fprintf(recipeFile, "\n");
	}
	printf("New recipe list saved successfully!\n");
	/* Removes the file from memory */
	fclose(recipeFile);
}

Ingredient *findIngredientNodeByName(Ingredient *root, char *name) {
    Ingredient* conductor = root;
    
    while(conductor->next != NULL) {
        conductor = conductor->next;
        if(strcmp(conductor->name, name) == 0)
            return conductor;
    }
    
    return NULL;
}

Recipe *findRecipeNodeByName(Recipe *root, char *name) {
    Recipe* conductor = root;
    
    while(conductor->next != NULL) {
        conductor = conductor->next;
        if(strcmp(conductor->name, name) == 0)
            return conductor;
    }
    
    return NULL;
}

Ingredient *addIngredientNode(Ingredient *root, Ingredient* node, int *numberOfIngredients) {
    Ingredient *current = root;
    while (current->next != NULL)
        current = current->next;
    current->next = node;
    (*numberOfIngredients)++;
    return current->next;
}

void removeIngredientNode(Ingredient *root, Ingredient* node, int *numberOfIngredients) {
	Ingredient *prev = root;
	Ingredient *next = root->next;
	Ingredient *current = root;
	prev = current;
	next = current->next;
	while(current != NULL){
		if(current == node){
			prev->next = current->next;
			free(current->name);
			free(current);
			(*numberOfIngredients)--;
			return;
		}
		prev = current;
		current = current->next;
	}
}

Recipe *addRecipeNode(Recipe *root, Recipe* node, int *numberOfRecipes) {
    Recipe *current = root;
    while (current->next != NULL)
        current = current->next;
    current->next = node;
    (*numberOfRecipes)++;
    return current->next;
}

void removeRecipeNode(Recipe *root, Recipe* node, int *numberOfRecipes) {
	Recipe *prev = root;
	Recipe *next = root->next;
	Recipe *current = root;
	prev = current;
	next = current->next;
	while(current != NULL){
		if(current == node){
			prev->next = current->next;
			free(current->name);
			free(current);
			(*numberOfRecipes)--;
			return;
		}
		prev = current;
		current = current->next;
	}
}

int digits(int number) {
	/* explanation: in order to get the LENGTH of the number in question we use log10, which we round down using the floor function.
	 * then we add one, because the length it returns is consistently one lower than the desired result. */
	return floor(log10(abs(number))) +1;
}

Ingredient* allocIngredient(char* name, int amount, double duration) {
    if(name == NULL)
        name = "";
    
    Ingredient* ingredient = malloc(sizeof(Ingredient));
    ingredient->name = malloc(strlen(name)+1);
    ingredient->amount = amount;
    ingredient->duration = duration;
    ingredient->next = NULL;
    
    strcpy(ingredient->name, name);
    
    return ingredient;
}

Recipe* allocRecipe(char* name, int ingredientCount) {
    if(name == NULL)
        name = "";
    
    Recipe* recipe = malloc(sizeof(Recipe));
    recipe->name = malloc(strlen(name)+1);
    recipe->ingredientCount = ingredientCount;
    recipe->ingredientList = malloc(ingredientCount * sizeof(Ingredient*));
    recipe->ingredientAmount = malloc(ingredientCount * sizeof(int));
    recipe->next = NULL;
    
    strcpy(recipe->name, name);
    
    return recipe;
}