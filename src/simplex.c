#include "simplex.h"

int recipePriority(Recipe* recipe);
int matrixFindpivotRowInColumn(Matrix* matrix, int pivotColumn);
int recipeIndex(Recipe** recipes, int recipeCount, Recipe* recipe);
int ingredientIndex(Ingredient** ingredients, int ingredientCount, Ingredient* ingredient);

Matrix* createSimplexMatrix(Recipe* rootRecipe, Ingredient* rootIngredient) {
    int i, r, c, rows, columns, recipeCount, ingredientCount;
    Matrix* m;
    Recipe* recipe;
    Ingredient* ingredient;
    Recipe** recipes;
    Ingredient** ingredients;
    
    // first we count the number of recipes and ingredients
    recipeCount = 0;
    recipe = rootRecipe;
    while(recipe != NULL) {
        recipeCount++;
        recipe = recipe->next;
    }
    
    ingredientCount = 0;
    ingredient = rootIngredient;
    while(ingredient != NULL) {
        ingredientCount++;
        ingredient = ingredient->next;
    }
    
    // then we sort everything into arrays for the simplex matrix
    recipes = malloc(recipeCount * sizeof(Recipe*));
    i = 0;
    recipe = rootRecipe;
    while(recipe != NULL) {
        recipes[i] = recipe;
        i++;
        recipe = recipe->next;
    }
    
    ingredients = malloc(ingredientCount * sizeof(Ingredient*));
    i = 0;
    ingredient = rootIngredient;
    while(ingredient != NULL) {
        ingredients[i] = ingredient;
        i++;
        ingredient = ingredient->next;
    }
    
    // with this information we create our simplex matrix
    rows = ingredientCount + 2; // 1 skal være 2
    columns = recipeCount + rows + 1;
    m = createMatrix(rows, columns);
    for(r = 0; r < rows; r++)
        for(c = 0; c < columns; c++)
            m->data[r][c] = 0.00;
    
    // constraints, usage per recipe is added and priority
    recipe = rootRecipe;
    while(recipe != NULL) {
        for(i = 0; i < recipe->ingredientCount; i++) {
            ingredient = recipe->ingredientList[i];
            m->data
                [ingredientIndex(ingredients, ingredientCount, ingredient)][recipeIndex(recipes, recipeCount, recipe)] 
                    = recipe->ingredientAmount[i];
        }
        m->data[rows - 1][recipeIndex(recipes, recipeCount, recipe)] += -recipePriority(recipe);
        recipe = recipe->next;
    }
    
    // constraints, limits per ingredient is added
    ingredient = rootIngredient;
    while(ingredient != NULL) {
        m->data[ingredientIndex(ingredients, ingredientCount, ingredient)][columns - 1] = ingredient->amount;
        
        ingredient = ingredient->next;
    }
	
    // identify matrix added
    for(i = 0; i < rows; i++) {
        m->data[i][recipeCount + i] = 1.00;
    }
    
	//adding the week factor thingy
	for(c=0;c<recipeCount;c++)
	{
        m->data[rows-2][c] = 1.00;
	}
    m->data[rows-2][columns-1] = 7.00;
	
    
    return m;
}

Matrix* solveSimplexMatrix(Matrix* matrix) {
    int r, c, i, pivotColumn, pivotRow;
    //matrixDebugPrint(matrix);
    pivotColumn = matrixFindMinColumnInRow(matrix, matrix->rows - 1);
    while (matrix->data[matrix->rows - 1][pivotColumn] < 0) {
        pivotRow = matrixFindpivotRowInColumn(matrix, pivotColumn);
        matrixCreatePivot(matrix, pivotRow, pivotColumn);
        pivotColumn = matrixFindMinColumnInRow(matrix, matrix->rows - 1);
        //matrixDebugPrint(matrix);
    }
    
    return matrix;
}

void optimizeFoodplan(Recipe* rootRecipe, Ingredient* rootIngredient, Recipe*** outRecipes, double** outRecipeMultiplier, int* outRecipeCount) {
    int i, r, c;
    Recipe* recipe;
    
    // first we count the number of recipes
    *outRecipeCount = 0;
    recipe = rootRecipe;
    while(recipe != NULL) {
        (*outRecipeCount)++;
        recipe = recipe->next;
    }
    
    // then we sort everything into arrays for the simplex matrix
    (*outRecipes) = malloc(*outRecipeCount * sizeof(Recipe*));
    i = 0;
    recipe = rootRecipe;
    while(recipe != NULL) {
        (*outRecipes)[i] = recipe;
        i++;
        recipe = recipe->next;
    }
    
    (*outRecipeMultiplier) = malloc(*outRecipeCount * sizeof(double));
    
    Matrix* matrix = solveSimplexMatrix(createSimplexMatrix(rootRecipe, rootIngredient));
    Matrix* A = matrixCreateSubset(matrix, 0, 0,                    matrix->rows - 1, *outRecipeCount);
    Matrix* b = matrixCreateSubset(matrix, 0, matrix->columns - 1,  matrix->rows - 1, 1);
    
    for(c = 0; c < A->columns; c++) {
        int hasPivot = 0;
        int pivotRow = -1;
        for(r = 0; r < A->rows; r++) {
            double v = A->data[r][c];
            if(abs(v) < DBL_EPSILON)
                continue;
            else if(abs(v) - 1 < DBL_EPSILON && pivotRow == -1) {
                pivotRow = r;
                hasPivot = 1;
                continue;
            } else {
                hasPivot = 0;
                break;
            }
        }
        if(hasPivot) {
            (*outRecipeMultiplier)[c] = b->data[pivotRow][0];
        } else {
            (*outRecipeMultiplier)[c] = 0.00;
        }
    }
}

int recipeIndex(Recipe** recipes, int recipeCount, Recipe* recipe) {
    int i;
    
    for(i = 0; i < recipeCount; i++) {
        if(recipes[i] == recipe)
            return i;
    }
    
    return -1;
}

int ingredientIndex(Ingredient** ingredients, int ingredientCount, Ingredient* ingredient) {
    int i;
    
    for(i = 0; i < ingredientCount; i++) {
        if(ingredients[i] == ingredient)
            return i;
    }
    
    return -1;
}

int matrixFindpivotRowInColumn(Matrix* matrix, int column) {
    int r, min = 0;
    
    if(matrix->rows <= 1)
        return min;
    
    for(r = 0; r < matrix->rows; r++) { //-1 efter rows
        if (abs(matrix->data[r][column]) < DBL_EPSILON)
            continue;
        else if (r == matrix->rows - 1)//1 ændres til 2
            continue;
        else if (matrix->data[r][matrix->columns - 1] / matrix->data[r][column] < matrix->data[min][matrix->columns - 1] / matrix->data[min][column])
            min = r;
    }
    
    return min;
}

int recipePriority(Recipe* recipe) {
    int i, j, p = 0;
    
    for(i = 0; i < recipe->ingredientCount; i++) {
        if(recipe->ingredientList[i]->duration < DBL_EPSILON || recipe->ingredientList[i]->amount < DBL_EPSILON)
            j = -1000000000;
        else
            j = ((8 / recipe->ingredientList[i]->duration) * (recipe->ingredientList[i]->amount / 100.00));
        p += j;
    }
    
    return p;
}