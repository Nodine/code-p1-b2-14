#include "linmath.h"

Matrix* createMatrix(int rows, int columns) {
	int r;
    
	Matrix* matrix;
	matrix = (Matrix*)malloc(sizeof(Matrix));
	matrix->columns = columns;
	matrix->rows = rows;
	matrix->data = (double**)malloc(rows * sizeof(double*));
	for (r = 0; r < rows; r++)
		matrix->data[r] = malloc(columns * sizeof(double));
    
	return matrix;
}

Matrix* copyMatrix(Matrix* from) {
	int r;
    
    Matrix* matrix = createMatrix(from->rows, from->columns);
	for (r = 0; r < from->rows; r++)
        memcpy(&matrix->data[r], &from->data[r], from->columns * sizeof(double));
    
	return matrix;
}

void freeMatrix(Matrix* matrix){
	int r;
    
    for(r = 0; r < matrix->rows; r++)
        free(matrix->data[r]);
    free(matrix->data);
    free(matrix);
}

void matrixRowDivide(Matrix* matrix, int row, double value) {
	int c;
	
	for (c = 0; c < matrix->columns; c++)
		matrix->data[row][c] /= value;
}

void matrixRowSubtract(Matrix* matrix, int left, int right, double factor) {
	int c;
	
	for (c = 0; c < matrix->columns; c++)
		matrix->data[left][c] -= matrix->data[right][c] * factor;
}

void matrixCreatePivot(Matrix* matrix, int row, int column) {
	int r;
	
	double pivotValue = matrix->data[row][column];
    
    // check if value is != 0.00
	if (abs(pivotValue) < DBL_EPSILON)
		return;
	
    // check if value is != 1.00
	if (abs(pivotValue - 1.00) > DBL_EPSILON)
		matrixRowDivide(matrix, row, pivotValue);
    
    // create pivot by subtracting the pivot row point from all other rows
	for (r = 0; r < matrix->rows; r++){
		if (r == row)
			continue;
		matrixRowSubtract(matrix, r, row, matrix->data[r][column]);
	}
}

int matrixFindMinColumnInRow(Matrix* matrix, int row) {
    int c, min = 0;
    
    if(row < 0 || row >= matrix->rows)
        return -1;
    
    if(matrix->columns == 1)
        return min;
    
    for(c = 1; c < matrix->columns; c++) {
        if(matrix->data[row][c] < matrix->data[row][min])
            min = c;
    }
    
    return min;
}

int matrixFindMinRowInColumn(Matrix* matrix, int column) {
    int r, min = 0;
    
    if(column < 0 || column >= matrix->columns)
        return -1;
    
    if(matrix->rows == 1)
        return min;
    
    for(r = 1; r < matrix->rows; r++) {
        if(matrix->data[r][column] < matrix->data[min][column])
            min = r;
    }
    
    return min;
}

Matrix* matrixCreateSubset(Matrix* matrix, int row, int column, int rows, int columns) {
    int r, c;
    Matrix* m = createMatrix(rows, columns);
    
    for(r = 0; r < rows; r++) {
        for(c = 0; c < columns; c++) {
            m->data[r][c] = matrix->data[r + row][c + column];
        }
    }
    
    return m;
}

void matrixDebugPrint(Matrix* matrix) {
    int r, c;
    
    printf(" === Matrix(%d,%d) ===\n", matrix->rows, matrix->columns);
    for(r = 0; r < matrix->rows; r++) {
        for(c = 0; c < matrix->columns; c++) {
            printf("%8.2f ", matrix->data[r][c]);
        }
        printf("\n");
    }
}