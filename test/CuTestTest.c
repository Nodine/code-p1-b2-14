#include <stdio.h>

#include "CuTest.h"
#include "..\src\linmath.h"
#include "..\src\data.h"
#include "..\src\simplex.h"

/*-------------------------------------------------------------------------*
 * Helper functions
 *-------------------------------------------------------------------------*/

 

/*-------------------------------------------------------------------------*
 * linmath test
 *-------------------------------------------------------------------------*/

void testMatrixCreation(CuTest* tc) {
    printf("Matrix creation ... ");
    Matrix* m = createMatrix(2, 2);
    // [ 2,   4;
    //   8,  16 ]
    m->data[0][0] = 2;
    m->data[0][1] = 4;
    m->data[1][0] = 8;
    m->data[1][1] = 16;
    
    CuAssertIntEquals(tc,  2, m->rows);
    CuAssertIntEquals(tc,  2, m->columns);
    CuAssertDblEquals(tc,  2, m->data[0][0], 0.00);
    CuAssertDblEquals(tc,  4, m->data[0][1], 0.00);
    CuAssertDblEquals(tc,  8, m->data[1][0], 0.00);
    CuAssertDblEquals(tc, 16, m->data[1][1], 0.00);
    printf("done!\n");
}

void testMatrixCopy(CuTest* tc) {
    printf("Matrix copying ... ");
    Matrix* old = createMatrix(2, 2);
    Matrix* new;
    // [ 2,   4;
    //   8,  16 ]
    old->data[0][0] = 2;
    old->data[0][1] = 4;
    old->data[1][0] = 8;
    old->data[1][1] = 16;
    
    new = copyMatrix(old);
    
    CuAssert(tc, "Pointers are the same.", new != old);
    CuAssertIntEquals(tc,  2, new->rows);
    CuAssertIntEquals(tc,  2, new->columns);
    CuAssertDblEquals(tc,  2, new->data[0][0], 0.00);
    CuAssertDblEquals(tc,  4, new->data[0][1], 0.00);
    CuAssertDblEquals(tc,  8, new->data[1][0], 0.00);
    CuAssertDblEquals(tc, 16, new->data[1][1], 0.00);
    printf("done!\n");
}

void testMatrixRowDivide(CuTest* tc) {
    printf("Matrix row dividing ... ");
    Matrix* m = createMatrix(2, 2);
    
    // [ 2,   4;
    //   8,  16 ]
    m->data[0][0] = 2;
    m->data[0][1] = 4;
    m->data[1][0] = 8;
    m->data[1][1] = 16;
    matrixRowDivide(m, 0, 2); // divide row 1 with 2.
    
    // [ 1,   2;
    //   8,  16 ]
    CuAssertDblEquals(tc,  1, m->data[0][0], 0.00);
    CuAssertDblEquals(tc,  2, m->data[0][1], 0.00);
    CuAssertDblEquals(tc,  8, m->data[1][0], 0.00);
    CuAssertDblEquals(tc, 16, m->data[1][1], 0.00);
    printf("done!\n");
}

void testMatrixRowSubtract(CuTest* tc) {
    printf("Matrix row subtracting ... ");
    Matrix* m = createMatrix(2, 2);
    
    // [ 2,   4;
    //   8,  16 ]
    m->data[0][0] = 2;
    m->data[0][1] = 4;
    m->data[1][0] = 8;
    m->data[1][1] = 16;
    matrixRowSubtract(m, 0, 1, 1.00); // subtract row 1 from row 0.
    
    // [-6, -12;
    //   8,  16 ]
    CuAssertDblEquals(tc, -6, m->data[0][0], 0.00);
    CuAssertDblEquals(tc, -12, m->data[0][1], 0.00);
    CuAssertDblEquals(tc,  8, m->data[1][0], 0.00);
    CuAssertDblEquals(tc, 16, m->data[1][1], 0.00);
    printf("done!\n");
}

void testMatrixCreatePivot(CuTest* tc) {
    printf("Matrix create pivot ... ");
    Matrix* m = createMatrix(2, 2);
    
    // [ 2,   4;
    //   8,  16 ]
    m->data[0][0] = 2;
    m->data[0][1] = 4;
    m->data[1][0] = 8;
    m->data[1][1] = 16; 
    matrixCreatePivot(m, 1, 0); // create pivot at (2;1)
    
    // [ 0,  0;
    //   1,  2 ]
    CuAssertDblEquals(tc, 0, m->data[0][0], 0.00);
    CuAssertDblEquals(tc, 0, m->data[0][1], 0.00);
    CuAssertDblEquals(tc, 1, m->data[1][0], 0.00);
    CuAssertDblEquals(tc, 2, m->data[1][1], 0.00);
    printf("done!\n");
}

void testMatrixFindMin(CuTest* tc) {
    printf("Matrix find min ... ");
    Matrix* m = createMatrix(2, 2);
    
    // [  2,  4;
    //   16,  8 ]
    m->data[0][0] = 2;
    m->data[0][1] = 4;
    m->data[1][0] = 16;
    m->data[1][1] = 8;
    
    CuAssertIntEquals(tc, -1, matrixFindMinColumnInRow(m, -1)); // out of bounds
    CuAssertIntEquals(tc,  0, matrixFindMinColumnInRow(m,  0));
    CuAssertIntEquals(tc,  1, matrixFindMinColumnInRow(m,  1));
    CuAssertIntEquals(tc, -1, matrixFindMinColumnInRow(m,  2)); // out of bounds
    CuAssertIntEquals(tc, -1, matrixFindMinRowInColumn(m, -1)); // out of bounds
    CuAssertIntEquals(tc,  0, matrixFindMinRowInColumn(m,  0));
    CuAssertIntEquals(tc,  0, matrixFindMinRowInColumn(m,  1));
    CuAssertIntEquals(tc, -1, matrixFindMinRowInColumn(m,  2)); // out of bounds
    printf("done!\n");
}

void testMatrixCreateSubset(CuTest* tc) {
    printf("Matrix subsetting ... ");
    Matrix* old = createMatrix(2, 2);
    Matrix* new;
    // [ 2,   4;
    //   8,  16 ]
    old->data[0][0] = 2;
    old->data[0][1] = 4;
    old->data[1][0] = 8;
    old->data[1][1] = 16;
    
    new = matrixCreateSubset(old, 1, 0, 1, 2);
    // [ 8,  16 ]
    CuAssert(tc, "Pointers are the same.", new != old);
    CuAssertIntEquals(tc,  1, new->rows);
    CuAssertIntEquals(tc,  2, new->columns);
    CuAssertDblEquals(tc,  8, new->data[0][0], 0.00);
    CuAssertDblEquals(tc, 16, new->data[0][1], 0.00);
    printf("done!\n");
}

void testSimplexSetup(CuTest* tc) {
    printf("Simplex creation ... \n");
    
    Ingredient* beef = allocIngredient("okse", 200, 1);
    Ingredient* pork = allocIngredient("svin", 400, 4);
    Ingredient* pasta = allocIngredient("pasta", 300, 30);
    
    beef->next = pork;
    pork->next = pasta;
    
    Recipe* recipe1 = allocRecipe("opskrift1", 2);
    recipe1->ingredientList[0] = beef;
    recipe1->ingredientAmount[0] = 100;
    recipe1->ingredientList[1] = pasta;
    recipe1->ingredientAmount[1] = 100;
    Recipe* recipe2 = allocRecipe("opskrift2", 2);
    recipe2->ingredientList[0] = pork;
    recipe2->ingredientAmount[0] = 100;
    recipe2->ingredientList[1] = pasta;
    recipe2->ingredientAmount[1] = 100;
    
    recipe1->next = recipe2;
    
    Matrix* m = createSimplexMatrix(recipe1, beef);
    m = solveSimplexMatrix(m);
    
    
    //matrixDebugPrint(simplexmatrix);
    
    printf(" - done!\n");
}
/*-------------------------------------------------------------------------*
 * main
 *-------------------------------------------------------------------------*/

CuSuite* CuGetSuite(void) {
	CuSuite* suite = CuSuiteNew();

	SUITE_ADD_TEST(suite, testMatrixCreation);
	SUITE_ADD_TEST(suite, testMatrixCopy);
    // testMatrixFree, not possible?
	SUITE_ADD_TEST(suite, testMatrixRowDivide);
	SUITE_ADD_TEST(suite, testMatrixRowSubtract);
	SUITE_ADD_TEST(suite, testMatrixCreatePivot);
	SUITE_ADD_TEST(suite, testMatrixFindMin);
	SUITE_ADD_TEST(suite, testMatrixCreateSubset);
	SUITE_ADD_TEST(suite, testSimplexSetup);

	return suite;
}
